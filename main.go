package main

import (
	"fmt"

	"gitlab.com/tluke12345/go-log-decrypt/pkg/crypto"
)

func main() {

	key := []byte("key")
	plaintext := []byte("lorem ipsum dolor sit amet, consectetur adipiscing elit. sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
	
	fmt.Println("key")
	fmt.Println(string(key))

	println("plaintext")
	fmt.Println(string(plaintext))

	ciphertext, iv, err := crypto.Encrypt(key, plaintext)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("ciphertext")
	fmt.Println(string(ciphertext))

	fmt.Println("iv")
	fmt.Println(iv)

	result, err := crypto.Decrypt(key, ciphertext, iv)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("result")
	fmt.Println(string(result))
	
}
