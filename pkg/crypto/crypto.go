package crypto

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5"
	"crypto/rand"
	"errors"
	"io"
)

// Encrypt encrypts a plaintext using the given key and returns the ciphertext.
func Encrypt(key, plaintext []byte) (ciphertext []byte, iv []byte, err error) {
	hashedkey := md5.Sum([]byte(key))
	block, err := aes.NewCipher(hashedkey[:])
	if err != nil {
		return nil, nil, err
	}

	// Pad the plaintext to be a multiple of the block size
	// the value of the padding is the number of bytes added
	padLength := aes.BlockSize - (len(plaintext) % aes.BlockSize)
	trailing := bytes.Repeat([]byte{byte(padLength)}, padLength)
	plaintext = append(plaintext, trailing...)

	// Create Initialization Vector
	iv = make([]byte, aes.BlockSize)
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return nil, nil, err
	}

	// Create the cipher
	mode := cipher.NewCBCEncrypter(block, iv)
	ciphertext = make([]byte, len(plaintext))
	mode.CryptBlocks(ciphertext, plaintext)

	return ciphertext, iv, nil
}

// Decrypt decrypts a ciphertext using the given key and returns the plaintext.
func Decrypt(key, ciphertext, iv []byte) ([]byte, error) {
	hashedkey := md5.Sum([]byte(key))
	block, err := aes.NewCipher(hashedkey[:])
	if err != nil {
		return nil, err
	}

	if len(ciphertext) < aes.BlockSize {
		return nil, errors.New("ciphertext too short")
	}

	mode := cipher.NewCBCDecrypter(block, iv)
	mode.CryptBlocks(ciphertext, ciphertext)

	// Remove padding
	dataLength := len(ciphertext)
	padLength := int(ciphertext[dataLength-1])
	ciphertext = ciphertext[:dataLength-padLength]

	return ciphertext, nil
}
